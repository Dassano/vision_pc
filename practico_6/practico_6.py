import numpy as np
import cv2
import math

x = 30
y = 100
angledeg = 10
anglerad = math.radians(angledeg)
img = cv2.imread('coronavirus.jpeg', 1)

def euclidean (img, anglerad, x, y):
    (h,w) = (img.shape[0], img.shape[1])
    print('Columnas: ' + str(w)+ ', ' + 'Filas: '+str(h))
    M = np.float32([[math.cos(anglerad), math.sin(anglerad), x],
                    [-math.sin(anglerad),math.cos(anglerad), y]])

    shifted = cv2.warpAffine(img, M, (w, h)) #recibe la imagen, M que es la matriz de transformacion y el tamaño de la imagen.
    cv2.imshow('euclidean_transf', shifted)
    cv2.waitKey()
    return shifted

euclidean(img,anglerad,x,y)

