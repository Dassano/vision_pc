import cv2
import numpy as np
i=0
x0 , y0 = -1, -1
x1, y1 = -1, -1
x2 , y2 = -1, -1
x3 , y3 = -1, -1


img = cv2.imread('calculadora_persp.jpg', 1) #Leo la imagen a escribir.
rows, cols, ch = img.shape #Obtengo numero de columnas y filas de la imagen a escribir
#rows1, cols1, ch1 = img_incrustada.shape #Obtengo numero de columnas y filas de la imagen a incrustar
def draw_points(event, x, y, flags, param): #Creo funcion para seleccionar puntos y guardar las posiciones de los pixeles en una nueva variable.
    global x0, y0, x1, y1, x2, y2, x3, y3, i
    if event == cv2.EVENT_LBUTTONDOWN: #Si aprieto el botton izq del mouse..
        if i == 0:
            cv2.circle(img, (x, y), 3, (255, 0, 0), -1) #Dibujo punto en la imagen a escribir.
            x0, y0 = x, y #Guardo posicion de pixeles donde dibuje el punto.
            i = i+1
            print('x0 = ' + str(x0) + ', ' +'y0 = ' + str(y0)) #Muestro la posicion del punto seleccionado.
        elif i == 1:
            cv2.circle(img, (x, y), 3, (255, 0, 0), -1)
            x1, y1 = x, y
            i = i+1
            print('x1 = ' + str(x1) + ', ' + 'y1 = ' + str(y1))

        elif i == 2:
            cv2.circle(img, (x, y), 3, (255, 0, 0), -1)
            x2, y2 = x, y
            i = i+1
            print('x2 = ' + str(x2) + ', ' + 'y2 = ' + str(y2))

        elif i == 3:
            cv2.circle(img, (x, y), 3, (255, 0, 0), -1)
            x3, y3 = x, y
            i = i+1
            print('x2 = ' + str(x2) + ', ' + 'y2 = ' + str(y2))

while(1):
    cv2.imshow('image', img) #Muestro imagen original.
    cv2.setMouseCallback('image', draw_points) #Le asigno la funcion de dibujar y guardar puntos a la imagen original.

    if cv2.waitKey(20) & 0xFF == 27: #Si aprieto escape se cierra el bucle
        break

    elif cv2.waitKey(10) & 0xFF == 103: #Si aprieto la g, pasa lo siguiente:
        pts1 = np.float32([[x0, y0], [x1, y1], [x2, y2],[x3,y3]]) #Creo arreglo numpy float de 32 bits con los 3 puntos seleccionados de la imagen original.
        pts2 = np.float32([[0, 0], [cols, 0], [cols,rows],[0, rows]]) #Creo arreglo numpy float de 32 bits con  3 puntos de la imagen a incrustar: esquina superior izq, esquina superior derecha y esquina inferior izquierda.
                        #[Vert.Izq.Sup], [Vert.Der.Sup],[Vert.Der.Inf],[Vert.Izq.Inf]
        matrix = cv2.getPerspectiveTransform(pts1, pts2) #Le paso los valores de pts1 y pts2 a la funcion encargada de devolverme la matriz afin.
        result = cv2.warpPerspective(img, matrix, (cols, rows)) #Le aplico la matriz afin encontrada anteriormente a la imagen incrustada, y le doy el tamaño (cols, rows), que es el tamaño de la imagen original (mas adelante nos servira)
        cv2.imshow('final', result)
        cv2.waitKey(0)
        break
cv2.destroyAllWindows()