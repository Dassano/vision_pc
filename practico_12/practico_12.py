import numpy as np
import cv2
img2 = cv2.imread('calculadoraLL.png')
img1 = cv2.imread('calculadoraRR.png')


#Defino un número mínimo de matches para cacular la homografía, es decir, que si no tengo mas de 10 puntos
#correspondientes, no se hará la homografía, lo ideal es tener 30 o 40 para un mejor funcionamiento.
MIN_MATCH_COUNT = 10

gray_1=cv2.cvtColor(img1,cv2.COLOR_BGR2GRAY)
gray_2=cv2.cvtColor(img2,cv2.COLOR_BGR2GRAY)



#Deefino el objeto que nos va a ayudar a encontrar los puntos caracteristicos con sus descriptores.
sift = cv2.xfeatures2d.SIFT_create()
#Calculo puntos caracteristicos y descriptores de la imagen 1.
kp1, dscr1 = sift.detectAndCompute(img1, None)
#Calculo puntos caracteristicos y descriptores de la imagen 2.
kp2, dscr2 = sift.detectAndCompute(img2, None)


#Defino el objeto matcher, funcion que me encontrara el descriptor mas cercano de la segudna imagen.
#Este va comparando cada uno de los descriptores de la primer  imagen con todos los descriptores de
#la segunda y busca el mas cercano.
matcher = cv2.BFMatcher(cv2.NORM_L2)
#En matches vamos a guardar las correspondencias. Usaremos knn (k near neighbours).
#Nos permitira encontrar los vecimos mas cercanos a cada uno de los descriptores. Para cada uno de los descriptores
#le asigna dos descriptres en la segunda imagen, los cuales son vecinos. Se hace esto para luego usar el criterio de Lowe.
matches = matcher.knnMatch (dscr1, dscr2, k=2)

#Ahora aplicamos criterio de Lowe para eliminar esos puntos que tengan una correspondencia similar en la segunda imagen, y
#asi evitar errores. Guardaremos los puntos buenos en el la lista good. En matches vamos a tener guardados los dos puntos
#mas cercanos encontrados que se corresponden con la imagen 1. los vamos a recorrer con un for, que dice: Si la distancia
#al descriptor con el que se matcho (correspondió) es menor que el 0.7 de la distancia, lo vamos a guardar. Sino se cumple esto,
#no lo vamos a guardar.



good = []
for m, n in matches:
    if m.distance < 0.7*n.distance:
        good.append(m)

#Ahora, si el tamaño de los puntos buenos encontrados es mayor al numero minimo, es decir que es 11 o mas, vamos a calular
#la homografia, sino no hacemos nada.

if (len(good) > MIN_MATCH_COUNT):
    #Definimos a src_pts como un arreglo numpy de 32 donde se guarda los valores  buenos de la primera imagen.
    #Ver que se recorre todos los puntos de la matriz good. El reshape es para que tome la forma que se pide
    #en la funcion findMomograpfy.
    src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape (-1, 1, 2)
    dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape (-1, 1 , 2)
    # Computamos la homografía con RANSAC
    H, mask = cv2.findHomography(dst_pts, src_pts, cv2.RANSAC, 5.0)

wimg2 = cv2.warpPerspective(img2, H, (1334, 810))
#Mezclamos ambas imágenes
alpha = 0.5
blend = np.array(wimg2*alpha + img1*(1-alpha), dtype=np.uint8)
cv2.imshow('Resultado', blend)
cv2.waitKey(0)
