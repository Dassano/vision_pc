import numpy as np
import cv2
import math

x = 30
y = 100
angledeg = 10
s = 0.5
anglerad = math.radians(angledeg)
img = cv2.imread('coronavirus.jpeg', 1)

def similaridad (img, anglerad, x, y,s):
    (h,w) = (img.shape[0], img.shape[1])
    print('Columnas: ' + str(w)+ ', ' + 'Filas: '+str(h))
    M = np.float32([[s*math.cos(anglerad), s*math.sin(anglerad), x],
                    [s*-math.sin(anglerad),s*math.cos(anglerad), y]])

    shifted = cv2.warpAffine(img, M, (w, h)) #recibe la imagen, M que es la matriz de transformacion y el tamaño de la imagen.
    cv2.imshow('euclidean_transf', shifted)
    cv2.waitKey()
    return shifted

similaridad(img,anglerad,x,y,s)
