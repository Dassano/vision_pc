
import cv2
import sys
import numpy as np

cap = cv2.VideoCapture(0)
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output.avi',fourcc, 20.0, (640,480))

while (cap.isOpened()):
    # Capture frame-by-frame
    ret, frame = cap.read()  #con el read capturamos cada imagen del video. El objeto me devuelve dos parametros:
                             #ret (variable que indica si la captura fue correcta o no) y frame (se guarda nuestra imagen).
    if ret == True: #chequeamos si la captura esta correcta.

        # Display the resulting frame
        cv2.imshow('Frame', frame) #mostramos en una ventana de nombre frame, la imagen.
        ancho = int(cap.get(3))
        alto = int(cap.get(4))

        if cv2.waitKey(25) & 0xFF == ord('q'):
            print('El ancho de la imagen es ' + str(ancho) + ' pixeles y el alto es ' + str(alto) + ' pixeles')
            break
    else:
        break

cap.release()
out.release()
cv2.destroyAllWindows()


#Este practico tambien debe ser ejecutado desde la consola.
#Abrir carpeta con la ubicacion del archivo y acceder a consola  con el sig. comando ->python practico_4.py video1.mp4