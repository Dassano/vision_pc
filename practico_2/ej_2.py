import cv2
img = cv2.imread('imagen_cv.png', 0)
cv2.imshow('curriculum',img)
umbral = 200

max_value = 255  # 255 = píxel negro
min_value = 0  # 0 = píxel blanco

for i, row in enumerate(img):
    for j, col in enumerate(row):  # Estos dos for recorren todos los píxeles de la imagen y verifica si el valor es mayor o menos a un umbral
        if img[i][j] >= umbral:
            img[i][j] = max_value
        else:
            img[i][j] = min_value

cv2.imwrite('imagenResultante1.png', img)
cv2.imshow('imagenResultante1', img)

k = cv2.waitKey()