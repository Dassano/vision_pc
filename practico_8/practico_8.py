import cv2
import numpy as np
i=0
x0 , y0 = -1, -1
x1, y1 = -1, -1
x2 , y2 = -1, -1

img = cv2.imread('argentina.jpg', 1) #Leo la imagen a escribir.
img_incrustada = cv2.imread('coronavirus_incrustada.jpg', 1) #Leo la imagen a incrustar.


rows, cols, ch = img.shape #Obtengo numero de columnas y filas de la imagen a escribir
rows1, cols1, ch1 = img_incrustada.shape #Obtengo numero de columnas y filas de la imagen a incrustar
def draw_points(event, x, y, flags, param): #Creo funcion para seleccionar puntos y guardar las posiciones de los pixeles en una nueva variable.
    global x0, y0, x1, y1, x2, y2, i
    if event == cv2.EVENT_LBUTTONDOWN: #Si aprieto el botton izq del mouse..
        if i == 0:
            cv2.circle(img, (x, y), 3, (255, 0, 0), -1) #Dibujo punto en la imagen a escribir.
            x0, y0 = x, y #Guardo posicion de pixeles donde dibuje el punto.
            i = i+1
            print('x0 = ' + str(x0) + ', ' +'y0 = ' + str(y0)) #Muestro la posicion del punto seleccionado.
        elif i == 1:
            cv2.circle(img, (x, y), 3, (255, 0, 0), -1)
            x1, y1 = x, y
            i = i+1
            print('x1 = ' + str(x1) + ', ' + 'y1 = ' + str(y1))

        elif i == 2:
            cv2.circle(img, (x, y), 3, (255, 0, 0), -1)
            x2, y2 = x, y
            i = i+1
            print('x2 = ' + str(x2) + ', ' + 'y2 = ' + str(y2))

while(1):
    cv2.imshow('image', img) #Muestro imagen original.
    cv2.setMouseCallback('image', draw_points) #Le asigno la funcion de dibujar y guardar puntos a la imagen original.

    if cv2.waitKey(20) & 0xFF == 27: #Si aprieto escape se cierra el bucle
        break

    elif cv2.waitKey(10) & 0xFF == 103: #Si aprieto la g, pasa lo siguiente:
        pts1 = np.float32([[x0, y0], [x1, y1], [x2, y2]]) #Creo arreglo numpy float de 32 bits con los 3 puntos seleccionados de la imagen original.
        pts2 = np.float32([[0, 0], [cols1, 0], [0, rows1]]) #Creo arreglo numpy float de 32 bits con  3 puntos de la imagen a incrustar: esquina superior izq, esquina superior derecha y esquina inferior izquierda.
        matrix = cv2.getAffineTransform(pts2, pts1) #Le paso los valores de pts1 y pts2 a la funcion encargada de devolverme la matriz afin.
        result = cv2.warpAffine(img_incrustada, matrix, (cols, rows)) #Le aplico la matriz afin encontrada anteriormente a la imagen incrustada, y le doy el tamaño (cols, rows), que es el tamaño de la imagen original (mas adelante nos servira)

        """Hasta aqui hemos aplicado la matriz afin a la imagen a incrustar, basandonos en los puntos seleccionados. Lo que sigue, es 
        pegar la imagen editada sobre la imagen original. Esto se hace con el procedimiento que se muestra a continuacion."""

        img2gray = cv2.cvtColor(result, cv2.COLOR_BGR2GRAY) #Paso la imagen editada a escala de grises.
        ret, mask = cv2.threshold(img2gray, 10, 255, cv2.THRESH_BINARY) #Aplico umbralizacion de la imagen. Si el pixel es mayor que 10, aplico un 255 (blanco). Recordar: numero mas grande, mas blanco es.
        mask_inv = cv2.bitwise_not(mask) #realizo una operacion not con la mascara original. Esto me devuelve la imagen inversa, es decir: donde era blanco ahora es negro y viceversa.
        cv2.imshow('image1', result)
        img1_bg = cv2.bitwise_and(img, img, mask = mask_inv) #realizo operacion and entre la imagen original y la mascara invertida. Se considera el color blanco como TRUE.
        img2_fg = cv2.bitwise_and(result, result, mask=mask) #realizo operacion and entre la imagen resultante y la mascara.
        dst = cv2.add(img1_bg, img2_fg) #sumo la imagen con la mascara y la img2_fg
        cv2.imshow('final', dst)
        cv2.waitKey(0)
        break
cv2.destroyAllWindows()
