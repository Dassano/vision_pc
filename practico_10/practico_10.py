import cv2
import numpy as np

def midpoint(ptA, ptB):
	return ((ptA[0] + ptB[0]) * 0.5, (ptA[1] + ptB[1]) * 0.5)


# load the image, convert it to grayscale, and blur it slightly
image = cv2.imread('objetos.jpg') #leemos la imagen
#aplicamos filtro gaussiano para eliminar ruidos
gray = cv2.GaussianBlur(image, (7, 7), 0)
#pasamos imagen a escala de grises
gray1 = cv2.cvtColor(gray, cv2.COLOR_BGR2GRAY)

#canny me dibuja los bordes
edged = cv2.Canny(gray1, 10, 180)
#aplico dos filtros mas que hacen que mejore la imagen.
edged = cv2.dilate(edged, None, iterations=1)
edged = cv2.erode(edged, None, iterations=1)

#findcConours me devuelve los contornos cerrados con los puntos correspondientes. Me devuelve una lista con dichos puntos.
#Vemos que esta configurado para detectar los bordes externos.
contours, hierarchy = cv2.findContours(edged, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)


#A prueba y error, hemos averiguado que tenemos 4 contornos, y que, el contorno numero 3, es el del papel glace.
#Creamos un for que recorra todos los puntos en X e Y de este contorno, y, con el tercer argumento, ponemos busque el maximo valor de X o Y.
#El argmin o argmax es el encargado de encontrar el minimo o maximo valor.
for x, contour in enumerate(contours):
	if x == 3:
		izquierda_abajo = tuple(contour[contour[:,:,0].argmin()][0])
		derecha_arriba= tuple(contour[contour[:,:, 0].argmax()][0])
		izquierda_arriba = tuple(contour[contour[:,:, 1].argmin()][0])
		derecha_abajo = tuple(contour[contour[:,:, 1].argmax()][0])

#Dado que los esquinas tenian minimos errores, las corregimos para que luego se pueda hacer la transformada correctamente.
izquierda_abajo_corregido = (izquierda_abajo[0],izquierda_abajo[1]+9)
derecha_abajo_corregido = (derecha_abajo[0]+5,derecha_abajo[1]-3)
derecha_arriba_corregido = (derecha_arriba[0],derecha_arriba[1]-3)

print("1)_Imagen original. Para continuar con el programa presione C.")
cv2.imshow('imagen_original', image)
cv2.waitKey(0) & 0xFF == 67


#Creamos un arreglo numpy con 4 puntos. Estos luego serán utilizados para encontrar la matriz de transformación.
pts1 = np.float32([[izquierda_arriba[0], izquierda_arriba[1]], [derecha_arriba_corregido[0], derecha_arriba_corregido[1]], [derecha_abajo_corregido[0], derecha_abajo_corregido[1]],[izquierda_abajo_corregido[0],izquierda_abajo_corregido[1]]]) #Creo arreglo numpy float de 32 bits con los 4 puntos seleccionados de la imagen original.
pts2 = np.float32([[izquierda_arriba[0], izquierda_arriba[1]], [derecha_arriba_corregido[0], izquierda_arriba[1]], [derecha_arriba_corregido[0],izquierda_abajo_corregido[1]],[izquierda_arriba[0], izquierda_abajo_corregido[1]]]) #Creo arreglo numpy float de 32 bits con  4 puntos de la imagen a incrustar: esquina superior izq, esquina superior derecha y esquina inferior izquierda.
#[Vert.Izq.Sup], [Vert.Der.Sup],[Vert.Der.Inf],[Vert.Izq.Inf]
#Buscamos la matriz de transformación con el conjunto de puntos encontrado.
matrix = cv2.getPerspectiveTransform(pts1, pts2) #Le paso los valores de pts1 y pts2 a la funcion encargada de devolverme la matriz afin.
#Aplicamos la matriz de transformacion a la imagen original
result = cv2.warpPerspective(image, matrix, (500, 500)) #Le aplico la matriz afin encontrada anteriormente a la imagen incrustada, y le doy el tamaño (cols, rows), que es el tamaño de la imagen original (mas adelante nos servira)
print("2)_Imagen corregida en perspectiva. Para continuar con el programa presione C.")
cv2.imshow('imagen_corregida_en_perspectiva', result)
cv2.waitKey(0) & 0xFF == 67
print("3)_Imagen final. Se han encerrado en un rectángulo los objetos encontrados, marcando sus puntos medios y extremos. Para finalizar el programa y ver las medidas obtenidas, vuelva a presionar C")


#Realizo el filtrado nuevamente a la imagen ya transformada
gray = cv2.GaussianBlur(result, (7, 7), 0)
gray1 = cv2.cvtColor(gray, cv2.COLOR_BGR2GRAY)
edged = cv2.Canny(gray1, 10, 180)
edged = cv2.dilate(edged, None, iterations=1)
edged = cv2.erode(edged, None, iterations=1)

#Encuentro los nuevos contornos
contours1, hierarchy = cv2.findContours(edged, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)


#Ingreso a un for recorriendo los 4 nuevos contornos encontrados
for c in contours1:
	#Habiendo visto anteriormente las ares, conocemos cada una de estas.  Con este bloque, salteamos el contorno de la pagina, que no lo necesitamos.


	if cv2.contourArea(c) == 27:
		continue
	#Copiamos la imagen original.
	orig = result
	#Con esta funcion, encontramos el minimo rectangulo que puede dibujarse alrededor de los contornos encontrados.
	box = cv2.minAreaRect(c)
	#Busco los puntos extremos del rectangulo hallado con la funcion anterior.
	box = cv2.boxPoints(box)
	#Lo transformo en numpy array.
	box = np.array(box, dtype="int")
	#Dibujo los contornos.
	cv2.drawContours(orig, [box.astype("int")], -1, (0, 255, 0), 2)


	#Selecciono el area del papel glace y busco la distancia de uno de los lados, en Y.
	if cv2.contourArea(c) == 40700.5:
		(tlC, trC, brC, blC) = box
		proporcion = (tlC[1] - blC[1])

	#Selecciono el area del papel glace y busco la distancia de uno de los lados, en Y
	if cv2.contourArea(c) == 19033.5:
		(tlT, trT, brT, blT) = box
		alto_lado_izq = tlT[1] - trT[1]
		largo_borde_sup = brT[0] - trT[0]

	#Selecciono el area de la goma y busco la longitud de los lados.
	if cv2.contourArea(c) == 21:
		(tlG, trG, brG, blG) = box
		alto_lado_izq_goma = tlG[1] - trG[1]
		largo_borde_sup_goma = brG[0] - trG[0]

	# Selecciono el area de la moneda y busco la longitud del diametro (a lo alto).
	if cv2.contourArea(c) == 2321.5:
		(tlM, trM, brM, blM) = box
		diametro_moneda = tlM[1] - trM[1]
		diametro_moneda1 = brM[0] - trM[0]


	#Dibujo los puntos medios de cada cuadrado, con un a linea al medio.
	for (x, y) in box:
		cv2.circle(orig, (int(x), int(y)), 5, (0, 0, 255), -1)
		(tl, tr, br, bl) = box
		#print (box)
		(tltrX, tltrY) = midpoint(tl, tr)
		(blbrX, blbrY) = midpoint(bl, br)
		(tlblX, tlblY) = midpoint(tl, bl)
		(trbrX, trbrY) = midpoint(tr, br)
	# draw the midpoints on the image
		cv2.circle(orig, (int(tltrX), int(tltrY)), 5, (255, 0, 0), -1)
		cv2.circle(orig, (int(blbrX), int(blbrY)), 5, (255, 0, 0), -1)
		cv2.circle(orig, (int(tlblX), int(tlblY)), 5, (255, 0, 0), -1)
		cv2.circle(orig, (int(trbrX), int(trbrY)), 5, (255, 0, 0), -1)

		cv2.putText(orig, 'Imagen final:', (0, 50), 5, 2, (255, 0, 0), 0)

	# draw lines between the midpoints
		cv2.line(orig, (int(tltrX), int(tltrY)), (int(blbrX), int(blbrY)),
			(255, 0, 255), 2)
		cv2.line(orig, (int(tlblX), int(tlblY)), (int(trbrX), int(trbrY)),
			(255, 0, 255), 2)
		cv2.imshow('imagen_final',orig)
cv2.waitKey(0) & 0xFF == 67

#Realizo la conversión y muestro dimensiones de la tarjeta.
alto_tarjeta = (alto_lado_izq*10)/(proporcion)
ancho_tarjeta = (largo_borde_sup*10)/(proporcion)

print('Alto tarjeta:' , '{0:.3f}'.format(alto_tarjeta) , 'cm')
print('Alto tarjeta:' , '{0:.3f}'.format(ancho_tarjeta) , 'cm')

#Realizo la conversión y muestro dimensiones de la goma.
alto_goma = (alto_lado_izq_goma*10)/(proporcion)
ancho_goma = (largo_borde_sup_goma*10)/(proporcion)

print('Alto de goma:' , '{0:.3f}'.format(alto_goma) , 'cm')
print('Ancho de goma:' , '{0:.3f}'.format(ancho_goma) , 'cm')
#Realizo la conversión y muestro dimensiones de la moned de $ 0.50.
diametro_moneda_cm = (diametro_moneda*10)/(proporcion)
print('Diametro moneda de $ 0.5:' , '{0:.3f}'.format(diametro_moneda), 'cm')

#Realizo la conversión y muestro dimensiones de la moned de $1.
diametro_moneda_cm1= ((diametro_moneda1/2)-2)*10/(proporcion)
print('Diametro moneda de $ 1:' , '{0:.3f}'.format(diametro_moneda_cm1) , 'cm')







