import sys
import cv2

if (len(sys.argv) > 1):
    filename = sys.argv [1]

else:
    print('Pass a filename as first argument')
    sys.exit(0)

cap = cv2.VideoCapture(filename)

while(cap.isOpened()):
    ret, frame = cap.read()

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    print (int(cap.get(5)))
    cv2.imshow('frame', gray)
    if((cv2.waitKey(33) & 0xFF) == ord('q')):
        break
cap.release()
cv2.destroyAllWindows()

#python practico_3.py video1.mp4 -> ejecutar asi desde consola (abierta desde la carpeta donde tengo el programa.py)
#preguntar poque me reproduce el video en 30 fps y no en 60 como es realmente el video